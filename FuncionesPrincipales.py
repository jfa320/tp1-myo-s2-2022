from builtins import print
from pyscipopt import *

def optimizar(drogasNecesarias,remediosDisponibles,remedioNuevo):
    print("---------------------------------")
    print("Input: ")
    print("Drogas necesarias: "+ str(drogasNecesarias))
    print("Remedios disponibles (farmacia): "+ str(remediosDisponibles))
    print("---------------------------------")
    print("Variables: ")
    print("x_i: dosis usada del remedio i en el coctel con i e {1,..., cantidad de remedios} ")
    print("---------------------------------")
    restriccionesDrogas=[]
    # --------------------
    # Creo el modelo
    model=Model("CoctelDrogas")
    model.hideOutput(True)
    #--------------------
    # Agrego variables al modelo
    i=1 #contador para nombrar variables
    for remedio in remediosDisponibles:
        nombreVariable="x"+str(i) #armo el nombre de las variables
        globals()[nombreVariable]=model.addVar(nombreVariable,vtype="C") #agrego la variable al modelo
        i+=1 #aumento el numero del contador

    # --------------------
    # defino funcion objetivo
    expresionFO=""
    remedioContador = 1
    #para la FO armo un string con las variables involucradas (remedios disponibles) y luego se lo sumo al modelo
    for remedio in remediosDisponibles:
        nombreVariable = "x" + str(remedioContador)
        expresionFO=expresionFO+nombreVariable+"+"
        remedioContador += 1  # aumento el numero del contador
    print("Funcion objetivo: min "+expresionFO[:-1])
    model.setObjective(eval(expresionFO[:-1]),"minimize") #eval convierte un string a una expresion
                                                        #[:-1] quita el ultimo caracter de la cadena
    # --------------------
    # defino restricciones
    print("---------------------------------")
    print("Sujeto a: ")
    #restriccion por cantidad de drogas necesarias para el cocteL
    expresionRestriccionDrogasNecesarias=""
    #la lógica de acá se encarga de leer los diccionarios que usamos para guardar en memoria las drogas necesarias
    #y los remedios disponible. Además, se va armando la restricción de cantidad de drogas en base al contenido
    #de cada remedio
    for drogaNecesaria in drogasNecesarias:
        cantidadNecesaria=drogasNecesarias[drogaNecesaria]
        i = 1
        for remedio in remediosDisponibles:
            encontreDosisRemedio = False
            for tipoDrogaEnRemedio in remediosDisponibles[remedio]:
                if(tipoDrogaEnRemedio==drogaNecesaria):
                    expresionRestriccionDrogasNecesarias+=str(remediosDisponibles[remedio][tipoDrogaEnRemedio])+"*"+"x"+str(i) + "+"
                    encontreDosisRemedio=True
            if (encontreDosisRemedio == False):
                expresionRestriccionDrogasNecesarias += str(0)+"*"+"x"+str(i) + "+"
            i+=1
        restriccionesDrogas.append(expresionRestriccionDrogasNecesarias[:-1] +">="+str(cantidadNecesaria))
        print("Imprimo restriccion para droga: "+str(drogaNecesaria))
        print(expresionRestriccionDrogasNecesarias[:-1] + ">=" + str(cantidadNecesaria))
        model.addCons(eval(expresionRestriccionDrogasNecesarias[:-1]) >= cantidadNecesaria)
        expresionRestriccionDrogasNecesarias=""
    # --------------------
    # restriccion de no negatividad de las variables
    # agrego esta restricción a cada variable involucrada
    i = 1
    print("Restricciones de no negatividad")
    for remedio in remediosDisponibles:
        nombreVariable = "x" + str(i)
        model.addCons(eval(nombreVariable) >= 0)
        i += 1  # aumento el numero del contador
        print(nombreVariable +" >= 0")
    print("---------------------------------")
    #optimizo el modelo
    model.optimize()
    #obtengo la solucion
    solucion=model.getBestSol()
    print("Solucion optima (SIN REMEDIO NUEVO): ")
    i = 1
    #si hay solucion óptima, la imprimo por pantalla sino muestro una advertencia indicando que no existe solución
    if(model.getStatus()!="infeasible"):
        for remedio in remediosDisponibles:
            nombreVariable = "x" + str(i)
            print(nombreVariable+": "+str(round(solucion[eval(nombreVariable)],4))) # redondeo en 4 decimales. Quitar round si no es necesario
            i += 1  # aumento el numero del contador
    else:
        print("NO SE ENCONTRARON SOLUCIONES FACTIBLES")
    #---------------------------------------------------
    #Desde acá reoptimizo usando lo anterior pero sumando un nuevo remedio
    model.freeTransform()
    print("---------------------------------")
    print("Voy a agregar nuevo remedio al modelo y recalcular la solución")
    print("Remedio nuevo: "+str(remedioNuevo))
    remediosDisponibles.update(remedioNuevo)  # agrego el remedio nuevo al diccionario viejo
    # --------------------------------------------------
    #Defino variable nueva
    nombreVariableRemedioNuevo = "x" + str(remedioContador)  # armo el nombre de la variable nueva
    globals()[nombreVariableRemedioNuevo] = model.addVar(nombreVariableRemedioNuevo, vtype="C")  # agrego la variable al modelo

    #---------------------------------------------------
    #Nueva FO
    #A la FO existente le sumo una variable nueva que representa al nuevo remedio
    expresionFO=expresionFO+"x"+str(remedioContador)+"+"
    print("---------------------------------")
    print("Funcion objetivo CON REMEDIO NUEVO: min " + expresionFO[:-1])
    print("---------------------------------")
    model.chgReoptObjective(eval(expresionFO[:-1]), "minimize")
    #--------------------------------------------------
    # restriccion por cantidad de drogas necesarias
    # a cada restriccion le agrego la variable y su coeficiente a la restricción
    # si es 0, simplemente agrego la variable con coeficiente 0 a la restricción
    i = 0
    for drogaNecesaria in drogasNecesarias:
        for remedio in remedioNuevo:
            encontreDosisRemedio = False
            for tipoDrogaEnRemedio in remediosDisponibles[remedio]:
                if(tipoDrogaEnRemedio==drogaNecesaria):
                    encontreDosisRemedio = True
                    #con addConsCoeff() modifico la restricción ya existente
                    model.addConsCoeff(model.getConss()[i], eval(nombreVariableRemedioNuevo), remediosDisponibles[remedio][tipoDrogaEnRemedio])

                if (encontreDosisRemedio == False):
                    model.addConsCoeff(model.getConss()[i], eval(nombreVariableRemedioNuevo), 0) #dosis del remedio sera 0

        i += 1

    # restriccion de no negatividad de la variable nueva
    model.addCons(eval(nombreVariableRemedioNuevo) >= 0)
    #-----------------------------------------------

    # optimizo el modelo
    model.optimize()
    # obtengo solucion
    solucion = model.getBestSol()
    print("Solucion optima (CON REMEDIO NUEVO): ")
    i = 1

    # si hay solucion óptima, la imprimo por pantalla sino muestro una advertencia indicando que no existe solución
    if (model.getStatus() != "infeasible"):
        for remedio in remediosDisponibles:
            nombreVariable = "x" + str(i)
            print(nombreVariable + ": " + str(
                round(solucion[eval(nombreVariable)], 4)))  # redondeo en 4 decimales. Quitar round si no es necesario
            i += 1  # aumento el numero del contador
    else:
        print("NO SE ENCONTRARON SOLUCIONES FACTIBLES")

def optimizarPunto1(drogasNecesarias,remediosDisponibles):
    print("Estoy en: optimizar()")
    print(drogasNecesarias)
    print(remediosDisponibles)
    restriccionesDrogas=[]
    # --------------------
    # creo el modelo
    model=Model("CoctelDrogas")
    #--------------------
    # agrego variables al modelo
    i=1 #contador para nombrar variables
    for remedio in remediosDisponibles:
        nombreVariable="x"+str(i) #armo el nombre de las variables
        globals()[nombreVariable]=model.addVar(nombreVariable,vtype="C") #agrego la variable al modelo
        i+=1 #aumento el numero del contador

    # --------------------
    # defino funcion objetivo
    expresionFO=""
    remedioContador = 1
    for remedio in remediosDisponibles:
        nombreVariable = "x" + str(remedioContador)
        expresionFO=expresionFO+nombreVariable+"+"
        remedioContador += 1  # aumento el numero del contador
    print("Funcion objetivo: "+expresionFO[:-1])
    model.setObjective(eval(expresionFO[:-1]),"minimize") #eval convierte un string a una expresion
                                                        #[:-1] quita el ultimo caracter de la cadena
    # --------------------
    # defino restricciones

    #restriccion por cantidad de drogas necesarias para el cocteL
    expresionRestriccionDrogasNecesarias=""
    for drogaNecesaria in drogasNecesarias:
        cantidadNecesaria=drogasNecesarias[drogaNecesaria]
        i = 1
        for remedio in remediosDisponibles:
            encontreDosisRemedio = False
            for tipoDrogaEnRemedio in remediosDisponibles[remedio]:
                if(tipoDrogaEnRemedio==drogaNecesaria):
                    expresionRestriccionDrogasNecesarias+=str(remediosDisponibles[remedio][tipoDrogaEnRemedio])+"*"+"x"+str(i) + "+"
                    encontreDosisRemedio=True
            if (encontreDosisRemedio == False):
                expresionRestriccionDrogasNecesarias += str(0)+"*"+"x"+str(i) + "+"
            i+=1
        print("Imprimo restriccion para droga: "+str(drogaNecesaria))
        print(expresionRestriccionDrogasNecesarias[:-1])
        restriccionesDrogas.append(expresionRestriccionDrogasNecesarias[:-1] +">="+str(cantidadNecesaria))
        model.addCons(eval(expresionRestriccionDrogasNecesarias[:-1]) >= cantidadNecesaria)
        expresionRestriccionDrogasNecesarias=""
    # --------------------
    # restriccion de no negatividad de las variables
    i = 1
    for remedio in remediosDisponibles:
        nombreVariable = "x" + str(i)
        model.addCons(eval(nombreVariable) >= 0)
        i += 1  # aumento el numero del contador

    #optimizo el modelo
    model.optimize()
    #obtengo solucion
    solucion=model.getBestSol()
    print("Solucion optima (SIN REMEDIO NUEVO): ")
    i = 1

    if(model.getStatus()!="infeasible"):
        for remedio in remediosDisponibles:
            nombreVariable = "x" + str(i)
            print(nombreVariable+": "+str(round(solucion[eval(nombreVariable)],4))) #redondeo en 4 decimales. Quitar round sino
            i += 1  # aumento el numero del contador
    else:
        print("NO SE ENCONTRARON SOLUCIONES FACTIBLES")

def Coctel(farmacia,remedio): # resuelve el ejercicio 2 (con remedio nuevo). Devuelve la respuesta al ejercicio 1
                            #   y al ejercicio 2 (reoptimizar)
    global remedioNuevo
    leerTxtFarmacia(farmacia) # lectura del primer archivo ("farmacia")
    leerTxtRemedioNuevo(remedio) # lectura del segundo archivo ("remedio nuevo")
    optimizar(drogasNecesarias,remediosDisponibles,remedioNuevo) #método que se encarga de optimizar usando los datos
                            #de los inputs pasados. Cada lector de archivos guarda los datos leidos en las variables que
                            #se pasan a este método

def CoctelPunto1(farmacia): # resuelve el ejercicio 1 (sin remedio nuevo). Ejecutar si es necesario
    leerTxtFarmacia(farmacia)
    optimizarPunto1(drogasNecesarias,remediosDisponibles)

def leerDatos():
    # Lee el input del usuario y lo manda a la funcion principal Coctel para que haga el resto del trabajo
    rutaFarmaciaTxt=input("Ingrese la ruta del archivo de texto (Farmacia):")
    rutaRemedioNuevoTxt = input("Ingrese la ruta del archivo de texto (Remedio nuevo):")
    Coctel(rutaFarmaciaTxt,rutaRemedioNuevoTxt)


#Métodos encargados de la lectura de archivos

def generarParametros(z,t):
    VarOrigen = z
    count = 0
    params = ""
    cv={}
    cvinterna={}

    params = ''.join(VarOrigen.replace(": ", ":"))
    params = ''.join(VarOrigen.replace("\n", ""))

    if t == 'd':
         clave,valor = params.split(":")
         cv[clave.strip()]=float(valor.replace(" ", ""))
    else:
        clavep, valorp = params.split(":")
        lista = valorp.split(",")

        for s in lista:
            params2 = ''.join(s)
            clave, valor = params2.split()
            cvinterna[clave] =  int(valor)  if int(float(valor)) == float(valor)  else float(valor)

        cv[clavep.strip()] = cvinterna

    return cv

def leerTxtFarmacia(ruta_txt):
    print("Leyendo: "+ruta_txt)
    try:
        archivo = open(ruta_txt, 'r')
        patharchivo = os.path.splitext(ruta_txt)
        contenido = archivo.readlines()
        global NombreFileEntrada
        NombreFileEntrada = patharchivo[0].split("/").pop()
        tipoParametro=''

        if contenido == '':  # validar si el archivo esta vacio
            print("El archivo seleccionado esta vacio por favor verifique su texto de entrada.")
        else:
            global drogasNecesarias
            global remediosDisponibles
            drogasNecesarias = {}
            remediosDisponibles = {}
            cont = 0

            for f in contenido:
                cont = 0
                if not f.__contains__("#") and f != '\n' :
                    if  f.__contains__("drogas") or f.__contains__("DROGAS") :
                        tipoParametro='d'
                        cont = 1
                    elif f.__contains__("remedios") or f.__contains__("REMEDIOS"):
                        tipoParametro='r'
                        cont = 1

                    if cont==0:
                        if tipoParametro=='d':
                            drogasNecesarias.update(generarParametros(f,tipoParametro))
                        else:
                            remediosDisponibles.update(generarParametros(f, tipoParametro))


    except ValueError:
        print("Error no controlado." + ValueError)

    finally:
        archivo.close()


def leerTxtRemedioNuevo(ruta_txt):
    print("Leyendo: " + ruta_txt)
    try:
        archivo = open(ruta_txt, 'r')
        patharchivo = os.path.splitext(ruta_txt)
        contenido = archivo.readlines()
        global NombreFileEntrada
        NombreFileEntrada = patharchivo[0].split("/").pop()
        tipoParametro=''

        if contenido == '':  # validar si el archivo esta vacio
            print("El archivo seleccionado esta vacio por favor verifique su texto de entrada.")
        else:
            global remedioNuevo
            remedioNuevo = {}
            cont = 0

            for f in contenido:
                cont = 0
                if not f.__contains__("#") and f != '\n':
                    if f.__contains__("drogas") or f.__contains__("DROGAS"):
                        tipoParametro = 'd'
                        cont = 1
                    elif f.__contains__("remedios") or f.__contains__("REMEDIOS"):
                        tipoParametro = 'r'
                        cont = 1

                    if cont == 0:
                        if tipoParametro == 'r':
                            remedioNuevo.update(generarParametros(f, tipoParametro))
                            break


    except ValueError:
        print("Error no controlado." + ValueError)

    finally:
        archivo.close()

